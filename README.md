# Filly

Filly is a micro gem used to add, update and save credit cards on Stripe servers. To experiment with that code, run `bin/console` for an interactive prompt.

## Our Sponsor

This project is sponsored by Zepho Inc. If you are interested in learning TDD in Ruby, you can redeem the coupon here: https://www.udemy.com/learn-test-driven-development-in-ruby/?couponCode=svr

## Features:

1. Create a new credit card for a customer (creating credit card for the first time)
2. Add a new credit card as the default credit card
2. Update credit card expiration date
3. Stripe credentials checker

## Version

Ruby 	   : 2.3.0
Stripe Gem : 1.25

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'filly'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install filly

## Usage

Refer the StripeGateway class in the sample Rails app: https://bitbucket.org/bparanj/striped

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

```ruby
$ruby -rminitest/pride test/credential_test.rb --verbose
$ruby -rminitest/pride test/credit_card_test.rb --verbose
```

or run rake:

```ruby
rake
```

to run all the tests.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/bparanj/filly.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

