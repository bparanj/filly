require "stripe"

require "filly/version"
require "filly/credit_card"
require "filly/credential"

module Filly
  STRIPE_API_VERSION = '2015-09-03 (latest)'
end
