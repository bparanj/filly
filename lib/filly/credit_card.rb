module Filly

  class CreditCard
    #
    # Adds a new credit card as the active default card for an existing customer
    #
    # stripe_customer_id : Stripe customer to add a new credit card 
    # stripe_token : Token representing the credit card
    #
    # Returns : Stripe::Card object
    #
    def self.add(stripe_customer_id, stripe_token)
      customer = Stripe::Customer.retrieve(stripe_customer_id)
      card = customer.sources.create(source: stripe_token)
      customer.default_card = card.id
      customer.save
      card
    end

    #
    # Create a new credit card for a customer
    #
    # stripe_token : Token representing the credit card
    # description  : The description for the transaction
    #
    # Returns : Stripe::Customer
    #
    def self.save(stripe_token, description)
      Stripe::Customer.create(card: stripe_token, description: description)
    end

    #
    # Update expiration date of an existing credit card for a customer
    #     
    # stripe_customer_id : Stripe customer to update the credit card expiration date
    # card_month         : The credit card expiration month
    # card_year          : The credit card expiration year
    #
    # Returns : Stripe::Card object
    #
    def self.update_expiration_date(stripe_customer_id, card_month, card_year)
      customer = Stripe::Customer.retrieve(stripe_customer_id)
      card = customer.sources.first
      card.exp_month = card_month
      card.exp_year = card_year
      card.save      
    end
    
  end

end