module Filly
  class Credential
    def self.check
      message = <<ERROR 
  Stripe credentials is not set properly or it is incorrect. 
  Define the following Stripe environment variables.

  export STRIPE_PUBLISHABLE_KEY='pk_test your publishable key' 
  export STRIPE_SECRET_KEY='sk_test your secret key'
  
  and define:
  
  Stripe.api_key = 'sk_test your secret key'
  
  in your code.
ERROR
      begin
        ::Stripe::Account.retrieve
        true
      rescue ::Stripe::AuthenticationError => exception
        puts message
        puts "*" * 40
        puts "Stripe publishable key : #{ENV['STRIPE_PUBLISHABLE_KEY']}"
        puts "Stripe secret key : #{ENV['STRIPE_SECRET_KEY']} "
        puts "Stripe.api_key : #{Stripe.api_key}"
        puts "*" * 40
        false
      end      
    end
  end
end

