require_relative 'test_helper'

class FillyTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Filly::VERSION
  end

  def test_that_it_has_stripe_api_version_number
    actual_version = ::Filly::STRIPE_API_VERSION
    
    expected_version = '2015-09-03 (latest)'
    
    assert_equal actual_version, expected_version
  end

end
