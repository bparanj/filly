require_relative 'test_helper'

class CredentialTest < Minitest::Test
    
  def test_stripe_credentials_incorrect_secret_key_returns_false
    Stripe.api_key = 'sk_test_ incorrect secret key'

    success = Filly::Credential.check

    refute success
  end

  # 
  # Set the environment variable before you run the test:
  # export STRIPE_SECRET_KEY='sk_test your secret key'
  #
  # This test will only pass if you provide the correct secret key.
  def test_stripe_credentials    
    Stripe.api_key = ENV['STRIPE_SECRET_KEY']
    
    success = Filly::Credential.check
    
    assert success
  end
  
end