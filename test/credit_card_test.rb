require_relative 'test_helper'

class CreditCardTest < Minitest::Test

  def setup
    Stripe.api_key = ENV['STRIPE_SECRET_KEY']
  end

  def test_add_new_credit_card_as_the_active_default_card
    token = Stripe::Token.create(:card => {
                                   :number    => "4242424242424242",
                                   :exp_month => 11,
                                   :exp_year  => 2025,
                                   :cvc       => "314"})
      
    customer = Stripe::Customer.create(:card => token.id, :description => "stripe-gateway-test@example.com")
    new_token = Stripe::Token.create(:card => {
                                       :number =>    "4012888888881881",
                                       :exp_month => 10,
                                       :exp_year =>  2035,
                                       :cvc =>       "123"})
  
    card = Filly::CreditCard.add(customer.id, new_token.id)
    
    assert_instance_of(Stripe::Card, card) 
  end
  
  
  
  
  def test_save_customer_credit_card_on_stripe_servers
    token = Stripe::Token.create(:card => {
                                    :number =>    "4242424242424242",
                                    :exp_month => 11,
                                    :exp_year =>  2025,
                                    :cvc =>       "314"})  

    customer = Filly::CreditCard.save(token.id, 'guest-user@example.com')
  
    refute_nil customer.id
  end
  
  def test_update_credit_card_expiration_date
    token = Stripe::Token.create(:card => {
                                    :number =>    "4242424242424242",
                                    :exp_month => 11,
                                    :exp_year =>  2025,
                                    :cvc =>       "314"})  
    customer = Stripe::Customer.create(card: token.id, description: "test-user@example.com")
    
    card = Filly::CreditCard.update_expiration_date(customer.id, 10, 2024)
    
    assert_equal card.exp_month, 10
    assert_equal card.exp_year, 2024
  end
  
end